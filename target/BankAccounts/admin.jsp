<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.*" %>
<%@ page import="connection.ConnectionDB" %>

<html>
<head>
    <title>Admin Profile</title>
    <style>
        <%@include file="css/admin"%>
    </style>
    <script>
        history.forward();
    </script>
</head>
<body>
<h1>Welcome to the Admin's page! You can manage Users' profiles.</h1>
<%
    try {
        Connection connection = ConnectionDB.getInstance().getConnection();
        PreparedStatement pstmt = connection.prepareStatement("SELECT * FROM users");
        ResultSet rs = pstmt.executeQuery();
%>
<table>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Status</th>
        <th>Action</th>
    </tr>
    <%
        while (rs.next()) {
            if (rs.getString(4).equals("user")) {
                if (rs.getString(5).equals("pending")) {
    %>
    <tr>
        <td><%=rs.getInt(1)%>
        </td>
        <td><%=rs.getString(2)%>
        </td>
        <td><%=rs.getString(5)%>
        </td>
        <td>
            <form action="activate">
                <input type="hidden" name="action" value="activate">
                <input type="hidden" name="id" value="<%= rs.getInt(1) %>"/>
                <input type="submit" value="Activate" style="height:30px">
                <br/>
            </form>
            <form action="block">
                <input type="hidden" name="action" value="block">
                <input type="hidden" name="id" value="<%= rs.getInt(1) %>"/>
                <input type="submit" value="Block" style="height:30px">
                <br/>
            </form>
            <form action="delete">
                <input type="hidden" name="action" value="delete">
                <input type="hidden" name="id" value="<%= rs.getInt(1) %>"/>
                <input type="submit" value="Delete" style="height:30px">
                <br/>
            </form>
        </td>
    </tr>
    <% } %>
    <% if (rs.getString(5).equals("active")) {
    %>
    <tr>
        <td><%=rs.getInt(1)%>
        </td>
        <td><%=rs.getString(2)%>
        </td>
        <td><%=rs.getString(5)%>
        </td>
        <td>
            <form action="block">
                <input type="hidden" name="action" value="block">
                <input type="hidden" name="id" value="<%= rs.getInt(1) %>"/>
                <input type="submit" value="Block" style="height:30px">
            </form>
        </td>
    </tr>
    <% } %>
    <% if (rs.getString(5).equals("blocked")) {
    %>
    <tr>
        <td><%=rs.getInt(1)%>
        </td>
        <td><%=rs.getString(2)%>
        </td>
        <td><%=rs.getString(5)%>
        </td>
        <td>
            <form action="unblock">
                <input type="hidden" name="action" value="unblock">
                <input type="hidden" name="id" value="<%= rs.getInt(1) %>"/>
                <input type="submit" value="Unblock" style="height:30px">
            </form>
        </td>
    </tr>
    <% } %>
    <% } %>
    <% } %>
    <%
    } catch (Exception ex) {
    %>
    <%
            out.println("Unable to connect to database.");
        }
    %>
</table>
<div>
    <a href='login.jsp'>Logout</a></div>

</body>
</html>
