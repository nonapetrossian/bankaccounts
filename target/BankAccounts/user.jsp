<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.*" %>
<%@ page import="model.User" %>
<%@ page import="connection.ConnectionDB" %>

<html>
<head>
    <title>User Profile</title>
    <script>
        history.forward();
    </script>
</head>
<body>
<h1 style="color:palevioletred">Welcome to Your page! You can see your Account Transactions.</h1>

<%
    User user = (User) session.getAttribute("userId");
    Connection connection = ConnectionDB.getInstance().getConnection();
    PreparedStatement pstm = connection.prepareStatement("select * from users where username = ?");
    pstm.setString(1, user.getUsername());
    ResultSet rs = pstm.executeQuery();
    // int id = rs.getInt(1);
    while(rs.next()) {
        out.println(rs.getString("username"));
    }
%>

<form action="accountRequest" method="post">
    <select name="currency">
        <option>AMD</option>
        <option>USD</option>
        <option>EURO</option>
        <option>RUB</option>
    </select>
    <br/><br/>
    <input type="hidden" name="id" value="<%= user %>" />
    <input type="submit" value="Submit" />
</form>
<br/>
<a href = 'login.jsp'>Logout</a>
</body>
</html>