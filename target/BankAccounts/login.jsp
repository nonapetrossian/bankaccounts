<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>
        <%@include file="css/login"%>
    </style>
</head>
<body>
<%
    if(request.getAttribute("registered") == "registered"){ %>
<p style="color:palevioletred">You have successfully registered! Please wait for activation.</p>
<% }
%>
<form action="login" method="post">
    <div class="container">
        <label for="username"><b>Username</b></label>
        <input type="text" placeholder="Enter Username" name="username" required>
        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="password" required>
        <button type="submit">Login</button>
    </div>
</form>
<br/>
<div>
    <a href='index.jsp'>Home</a>
</div>

</body>
</html>
