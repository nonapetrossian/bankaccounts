<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Blocked</title>
</head>
<body>
<h3 style="text-align: center; color:palevioletred"> Sorry, your account is blocked! </h3>
<br/>

<div style="text-align: center;
    font-size: 20px;
    display: block;
    margin: 0 auto;
    color: #4CAF50;
    text-decoration: none;">
    <a href = "index.jsp">Home</a>
</div>

</body>
</html>