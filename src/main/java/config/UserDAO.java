package config;

import connection.ConnectionDB;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserDAO {

    public static int save(User user) {

        int status = 0;
        if (!isExist(user.getUsername())) {
            try {
                Connection connection = ConnectionDB.getInstance().getConnection();
                PreparedStatement stmt = connection.prepareStatement(
                        "insert into users(username, password, role, status) " + "values(?,?,?,?)");
                stmt.setString(1, user.getUsername());
                stmt.setString(2, user.getPassword());
                stmt.setString(3, "user");
                stmt.setString(4, "pending");
                status = stmt.executeUpdate();
                status++;
                ConnectionDB.getInstance().close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return status;
    }

    private static boolean isExist(String username) {
        try {
            Connection connection = ConnectionDB.getInstance().getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT username FROM users where username=?");
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next())
                return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String update(User user) {
        int count = 0;
        String status = " ";
        try {
            Connection connection = ConnectionDB.getInstance().getConnection();
            PreparedStatement pstmt = connection.prepareStatement("SELECT * FROM users WHERE username = ? AND password = ?");
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, user.getPassword());
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                if (rs.getString("role").equals("admin")) {
                    status = "admin";
                } else {
                    if (rs.getString("status").equals("active")) {
                        status = "active";
                    }
                    if (rs.getString("status").equals("blocked")) {
                        status = "blocked";
                    }
                    if (rs.getString("status").equals("pending")) {
                        status = "pending";
                    }
                }
                count++;
            }
            if (count == 0) {
                System.out.println("eeee");
                status = "notFound";
            }
            ConnectionDB.getInstance().close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return status;
    }

    public static Integer delete(Integer id) {
        int status = 0;
        try {
            Connection connection = ConnectionDB.getInstance().getConnection();
            PreparedStatement pstmt = connection.prepareStatement("delete from users WHERE id = ?");
            pstmt.setInt(1, id);
            status = pstmt.executeUpdate();
            ConnectionDB.getInstance().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return status;
    }

    public static void activate(Integer id) {
        try {
            Connection connection = ConnectionDB.getInstance().getConnection();
            PreparedStatement pstmt = connection.prepareStatement("update users set status = 'active' where id = ?");
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            ConnectionDB.getInstance().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void block(Integer id) {
        try {
            Connection connection = ConnectionDB.getInstance().getConnection();
            PreparedStatement pstmt = connection.prepareStatement("update users set status = 'blocked' where id = ?");
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            ConnectionDB.getInstance().close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void unblock(Integer id) {
        activate(id);
    }
}
