package servlets;

import config.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/")
public class ServletManageStatus extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String option = request.getParameter("action");

        Integer id = Integer.valueOf((request.getParameter("id")));

        switch (option) {
            case "activate":
                UserDAO.activate(id);
                break;
            case "block":
                UserDAO.block(id);
                break;
            case "delete":
                UserDAO.delete(id);
                break;
            case "unblock":
                UserDAO.unblock(id);
                break;
        }
        request.getRequestDispatcher("admin.jsp").include(request, response);
    }
}
