package servlets;

import config.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registration")
public class ServletRegistration extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = "";
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        if (UserDAO.save(user) > 0) {
            request.setAttribute("registered", "registered");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            page = "/isExist.jsp";
        }
        response.sendRedirect(page);

    }
}