package servlets;

import config.UserDAO;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class ServletLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String page = "";

        User user = new User();

        user.setUsername(username);
        user.setPassword(password);
        String status = UserDAO.update(user);
        switch (status) {
            case "admin":
                page = "admin.jsp";
                break;
            case "active":
                HttpSession session = request.getSession();
                session.setAttribute("userId", user);
                page = "user.jsp";
                break;
            case "blocked":
                page = "/blocked.jsp";
                break;
            case "pending":
                page = "/pending.jsp";
                break;
            case "notFound":
                page = "/notFound.jsp";
                break;
        }
        response.sendRedirect(page);
    }
}
