package connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionDB {

    private static ConnectionDB connectionDB = null;
    private String driverName;
    private String username;
    private String password;
    private String url;

    private Connection conn = null;

    private ConnectionDB() {
    }

    public static ConnectionDB getInstance() {
        if (connectionDB == null)
            connectionDB = new ConnectionDB();
        return connectionDB;
    }

    public Connection getConnection() {
        try {
            readConnectionParams();
            Class.forName(driverName).getDeclaredConstructor().newInstance();
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

    private void readConnectionParams() {
        InputStream inputStream = ConnectionDB.class.getClassLoader().getResourceAsStream("properties/connectionData");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        driverName = properties.getProperty("driverName");
        url = properties.getProperty("URL");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
    }

    public void close() {
        System.out.println("Closing connection");
        try {
            conn.close();
        } catch (SQLException e) {
        }
        conn = null;
    }
}

